# Project-Manager

El siguiente proyecto reto esta basado en la implementación de un manejado de proyectos de
software basado en la metodología scrum.

Integrantes:
- Aldo Fabian Martinez Meza
- Miguel Omar Burillo Aguirre
- Brandon Haziel Ceballos Buenrostro
- Valeria Martinez Garibay


**Diagrama de Clases**


![Diagrama de clases](https://gitlab.com/Haziel01/package_manager/-/raw/main/test/Diagrama_Clases.png?ref_type=heads)

Nuestro diagrama de clases como su nombre nos indica, nos muestra las clases que contiene nuestro proyecto, asi como tambien se muestran sus atributos y el estado en el que se encuentran, cabe mencionar que algunas de las clases pueden estar relacionadas entre si asi como una clase puede tener un valor heredado de otra.


**Diagrama de Interacción**


![Diagrama de Interaccion](https://gitlab.com/a338823/package_manager1/-/raw/main/diagrama_secuencia.jpg)

El diagrama de Interaccion muestra la manera en la que el usuario podrá interactuar con la aplicación, la cual se muestra de una manera secuencial, paso a paso, se va mostrando las cosas que el usuario puede hacer, incluyendo las restricciones que la aplicación le marca.

**Docker Hub Imagen**
**Imagen en docker hub:** https://hub.docker.com/repository/docker/haziel01/project_manager/general
**docker pull:** docker pull haziel01/project_manager
